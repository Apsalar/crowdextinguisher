-- Allow debugging
require("mobdebug").start()

-- Hide Status Bar

display.setStatusBar(display.HiddenStatusBar)

-- Utils
function ternary ( cond , T , F )
    if cond then return T else return F end
end

-- Physics

local physics = require('physics')
physics.start()
physics.setGravity( 0, 0 )
--physics.setDrawMode("hybrid")

-- Enemy Generator

local enemyGenerator = require('enemyGenerator')
local collisions = require('collisions')

-- Graphics

-- [Background]

local bg = display.newImage('img/bg.png', 160, 240)


INITIAL_ENERGY = 700
myEnergy = INITIAL_ENERGY 
gameOverFlag = false

currentLevel = 1
local nextLevel = 100

-- [Title View]

local titleBg
local startBtn
local creditsBtn
local titleView

-- [Credits]

local creditsGroup
local text
local backBtn
local slideTime = 350
local scaleOnStart = 21
local scaleTime = 500

local lastY
local points = 0
local pointsBonus = 0
-- [Game]

local counterText = 0
local timeCounter

-- Functions

local Main = {}
local startButtonListeners = {}
local showCredits = {}
local hideCredits = {}
local showGameView = {}
local gameListeners = {}
local update = {}

local player
local playerGroup
local lastShotTime = 0
local menuMusic = audio.loadSound("snd/Avgvst - Mushrooms_1.ogg")
local inGameMusic = audio.loadSound("snd/Gregoire Lourme - Commando Team (Action) [loop cut].ogg")
local endGameMusic = audio.loadSound("snd/gameover.ogg")
local shotSound = audio.loadSound("snd/gunshot.ogg")
local hitSound = audio.loadSound("snd/ouch.mp3")
local fightSound = audio.loadSound("snd/fight.mp3")
local impressiveSound = audio.loadSound("snd/impressive.mp3")

function onShotOffScreen(self, event)
	display.remove(self)
end

function onTouch(event)

	if system.getTimer() > lastShotTime then
		local shot = display.newImage("img/bullet.png", display.contentCenterX, display.contentCenterY)
		shot.enterFrame = moveShot	
		player.rotation = (getImageRotation(event.x,event.y))
		shot.rotation = player.rotation
		physics.addBody( shot, "static", { density=0.1, friction=0.1, radius=20 } )
		transition.to( shot, { time=200, x=event.x, y=event.y, onComplete=onShotOffScreen } )
		audio.play( shotSound )
		Runtime:addEventListener("enterFrame", shot)
		lastShotTime = system.getTimer() + 100
    	shot.myType = "shot"
	end

end

function getImageRotation(x, y)
    local deltaY = y - display.contentCenterY
    local deltaX = x - display.contentCenterX

    local angleInDegrees = (math.atan2( deltaY, deltaX) * 180 / math.pi)
    --print(angleInDegrees)
    angleInDegrees = angleInDegrees + 90
	return angleInDegrees
end


local runtime = 0
function getDeltaTime()
   local temp = system.getTimer()  --Get current game time in ms
   local dt = (temp-runtime) / (1000/60)  --60fps or 30fps as base
   runtime = temp  --Store game time
   return dt
end


-- Main Function

function Main()		
	titleBg = display.newImage('img/title.png', display.contentCenterX, 96)
	
	startBtn = display.newImage('img/startBtn.png', display.contentCenterX, display.contentCenterY + 10)
	creditsBtn = display.newImage('img/creditsBtn.png', display.contentCenterX, display.contentCenterY + 65)
	
	titleView = display.newGroup(titleBg, startBtn, creditsBtn)
	
	startButtonListeners('add')
  	audio.play( menuMusic )
  	myEnergy = INITIAL_ENERGY 

	
end

function startButtonListeners(action)
	if(action == 'add') then
		startBtn:addEventListener('tap', showGameView)
		creditsBtn:addEventListener('tap', showCredits)
	else
		startBtn:removeEventListener('tap', showGameView)
		creditsBtn:removeEventListener('tap', showCredits)
	end
end

function showCredits:tap(e)
	startBtn.isVisible = false
	creditsBtn.isVisible = false
	creditsGroup = display.newGroup()

	text = display.newText( "Hackaton", display.contentWidth * 0.5, display.contentHeight, "Helvetica", 18 )
	text:setFillColor(255, 148, 0, 50)

	creditsGroup:insert( text )
	
	backBtn = display.newImage('img/backBtn.png', display.contentWidth * 0.5, display.contentHeight * 0.8)
	
	lastY = titleBg.y
	transition.to(titleBg, {time = slideTime, y = (display.contentHeight * 0.5) - (titleBg.height + 50)})
	transition.to(text, {time = slideTime, y = (display.contentHeight * 0.5) + 35, onComplete = function() backBtn:addEventListener('tap', hideCredits) end})
end

function hideCredits:tap(e)
	display.remove(backBtn)
	transition.to(text, {time = slideTime, y = display.contentHeight + 25, onComplete = function() creditsBtn.isVisible = true startBtn.isVisible = true text:removeEventListener('tap', hideCredits) display.remove(text) text = nil end})
	transition.to(titleBg, {time = slideTime, y = lastY});
end

---------------------------------------
--START GAME
function showGameView:tap(e)
	transition.to(titleView, {time = scaleTime, x = -display.contentWidth * 3, y = -display.contentHeight * 0.8, xScale = titleView.xScale * scaleOnStart, yScale = titleView.yScale * scaleOnStart, onComplete = function() startButtonListeners('remove') display.remove(titleView) titleView = nil end})
	
  display.remove(bg)
  local arena = display.newImage('img/arena.png', 160, 240)
  
	counterText = display.newText('0', display.contentWidth * 0.8, display.contentHeight * 0.05, "Helvetica", 18)
	scoreText = display.newText('0', display.contentWidth * 0.2, display.contentHeight * 0.05, "Helvetica", 18)
  scoreText.text = 'score: 0' 
	timeCounter = gameCounter()
	gameListeners('add')


  myEnergyTab = display.newRect( 0, 0, myEnergy, 5 )
  myEnergyTab:setFillColor(0, 255, 0, 255)

	player = display.newImage("img/player.png", display.contentCenterX, display.contentCenterY + 10)
  player.myType = "player"
  playerGroup = display.newGroup()
  playerGroup:insert(1, player)
  physics.addBody(player, "static", { density=0.8, friction=1.0, bounce=.7 })
	Runtime:addEventListener("touch", onTouch)	
  audio.stop( )
  audio.play( inGameMusic , {loops=-1})
  	audio.play( fightSound )

end

function gameListeners(action)
	if(action == 'add') then
		Runtime:addEventListener('enterFrame', update)
	else
		Runtime:removeEventListener('enterFrame', update)
	end
end

function gameCounter()
	local start = os.time()
	return function()
		local diff = os.difftime(os.time(), start)
		return diff
	end
end

---MAIN GAME LOOP
local lastTimeCounterValue
function update()
  --counterText.text = 'time: ' .. timeCounter();
  counterText.text = 'level: ' .. currentLevel
  if (timeCounter() ~= lastTimeCounterValue) then
    lastTimeCounterValue = timeCounter()
    enemyGenerator.Generate(currentLevel * 0.3 + 1)    
  end

	if system.getTimer() > lastShotTime then
		lastShotTime = 0

	end  
end

function bonusComplete(event)
	display.remove( event.target )
end

function calculateLevel()
  if (points >= nextLevel) then
    currentLevel = currentLevel + 1
    nextLevel = nextLevel * 1.5
  end
end

local function onCollision( event )
        if ( event.phase == "began" ) then
                if((event.object1.myType == "shot" and event.object2.myType =="enemy") or (event.object2.myType == "shot"   and event.object1.myType =="enemy")) then
                    
                    local bullet = ternary(event.object2.myType == "shot", event.object2, event.object1)
                    local victim = ternary(event.object2.myType == "enemy", event.object2, event.object1)
                   
                    display.remove(bullet)
                    victim.hp = victim.hp -1;
                    
                    if (victim.hp == 0) then
                      display.remove(victim)
                      points = points + victim.points
                      calculateLevel()
                      scoreText.text = 'score: ' .. points
                      pointsBonus = pointsBonus + victim.points
                      if (pointsBonus > 250) then
                      		pointsBonus = 0
                      		audio.play( impressiveSound )                   		
                      end
                      local bloodyPlace = display.newImage("img/bloodyPlace.png", victim.x, victim.y)
                      transition.to(bloodyPlace, {time = 4000, alpha = 0})                      

                      playerGroup:toFront()
                    end
                 end
                 
                 if ( isTurretHit(event.object1, event.object2) ) then
                  collisions.onTurretHit(event.object1, event.object2)
                 end
                --print( "began: " .. event.object1.myType .. " & " .. event.object2.myType )

    	if ( isShotAndEnemyHit(event.object1, event.object2) ) then
			local bloodSheetData = { width=64, height=64, numFrames=6, sheetContentWidth=384, sheetContentHeight=64 }
			local bloodSheet = graphics.newImageSheet("img/bloodResized.png", bloodSheetData)
	    local blood = display.newSprite(bloodSheet, { name = "normalRun", start = 1, count = 6, time = 200, loopCount = 1, loopDirection = "forward" })
	    	local enemy = getEnemy(event.object1, event.object2)
			blood.x = enemy.x
			blood.y = enemy.y
			blood:addEventListener( "sprite", bloodListener )
			audio.play( hitSound )
			blood:play()    		
    	end
  end
end

function bloodListener( event )
 
	if ( event.phase == "ended" ) then
	    display.remove( event.target )
  	end
 
end

function isShotAndEnemyHit(obj1, obj2)
	if (obj1.myType == "shot" and obj2.myType == "enemy") or (obj1.myType == "enemy" and obj2.myType == "shot") then
		return true;
	else
		return false;
	end
end

function isTurretHit(obj1, obj2)
	if (obj1.myType == "player" and obj2.myType == "enemy") or (obj1.myType == "enemy" and obj2.myType == "player")then
		return true;
	else
		return false;
	end
end


function getEnemy(obj1, obj2)
	if obj1.myType == "enemy" then
		return obj1;
	elseif obj2.myType == "enemy" then
		return obj2;
	else
		return nil
	end
end
 
Runtime:addEventListener( "collision", onCollision )

function gameOver()
	if ( gameOverFlag == false) then
		gameOverFlag = true
		print("Game Over")
	  	audio.stop()
	  	audio.play(endGameMusic)
	  	gameListeners("it is over")
	  	display.newImage( 'img/blood.png', 360, 340)
	  	Runtime:removeEventListener( "touch", onTouch)
	  	timer.performWithDelay( 1000, Main(), 0 )
	 end
end

Main()
