local collisions = {}

local energyUnit = 1

function collisions.onTurretHit (obj1, obj2)
	local enemy
	if (obj1.myType == "enemy") then
		enemy = obj1
	elseif (obj2.myType == "enemy") then
		enemy = obj2
	end
  local energyDelta = energyUnit*enemy.points
	myEnergy = myEnergy - energyDelta
  pointsBonus = 0
	if (myEnergy<0) then
		gameOver()  
	end
	display.remove( myEnergyTab )
	myEnergyTab = display.newRect( 0, 0, myEnergy, 5 )
  if (myEnergy < INITIAL_ENERGY *0.3) then
    myEnergyTab:setFillColor(255, 0, 0, 255)
  elseif (myEnergy < INITIAL_ENERGY *0.6) then
    myEnergyTab:setFillColor(255, 210, 0, 255)
  else
    myEnergyTab:setFillColor(0, 255, 0, 255)
  end
end

return collisions