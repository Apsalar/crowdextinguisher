local enemyGenerator = {}

enemyGenerator.target = {x = display.contentCenterX,
                         y = display.contentCenterY}

enemyGenerator.offset = 30;

enemyGenerator.types = {
  regular = { speed = 5000, sheetName = 'enemy', points = 10, enemyType = 'regular', hp = 1,
    sheetData = { width=16, height=16, numFrames=3, sheetContentWidth=48, sheetContentHeight=16 }},
  veteran = { speed = 7000, sheetName = 'veteran', points = 30, enemyType = 'veteran', hp = 2,
    sheetData = { width=25, height=25, numFrames=3, sheetContentWidth=75, sheetContentHeight=25 }},
  armouredVeteran = { speed = 9000, sheetName = 'armouredveteran', points = 50, enemyType = 'veteran', hp = 3,
    sheetData = { width=25, height=25, numFrames=3, sheetContentWidth=75, sheetContentHeight=25 }}
  }

function createTypeEnemy(eType)
  return function (enemy)
      local randomPoint = setRandomPoint()
      
      local enemySheetData = eType.sheetData
			local enemySheet = graphics.newImageSheet('img/' .. eType.sheetName .. '.png', enemySheetData)
	    local enemy = display.newSprite(enemySheet, { name = "normalRun", start = 1, count = 3, time = 300})
      
      enemy.x = randomPoint.x
      enemy.y = randomPoint.y
      --enemy = display.newSprite('img/' .. eType.sheetName .. '.png', randomPoint.x, randomPoint.y);
      enemy.points = eType.points
      enemy.enemyType = eType.enemyType
      enemy.hp = eType.hp
      enemy.myType = 'enemy'
      physics.addBody(enemy, enemyBody)
      enemy.rotation = calculateAngleFromTarget(randomPoint.x, randomPoint.y);
      transition.to(enemy, { time = eType.speed, x = enemyGenerator.target.x, y = enemyGenerator.target.y,
        onComplete = function() display.remove(enemy) enemy = nil end})
      enemy:play()
    return enemy
  end
end


local enemyBody = { density=0.8, friction=1.0, bounce=.7, radius=5 }
local createEnemy = {
  createTypeEnemy(enemyGenerator.types.regular),
  createTypeEnemy(enemyGenerator.types.veteran),
  createTypeEnemy(enemyGenerator.types.armouredVeteran)
}
--local createRegular = 
--local createVeteran = 
--local createArmouredVeteran = 

local enemy

function enemyGenerator.Generate(enemyCount)
  for i=1,enemyCount,1 do
    local upperLimit = 1
    if (currentLevel > 5) then
      upperLimit = 3
    elseif (currentLevel > 3) then
      upperLimit = 2
    end
    
    local et = math.random(1, upperLimit);
    createEnemy[et]()
  end 
end


function calculateAngleFromTarget(x, y)
  local deltaY = enemyGenerator.target.y - y;
  local deltaX = enemyGenerator.target.x - x;
  return math.atan2(deltaY, deltaX) * 180 / math.pi
end

function setRandomPoint()
    local point = {}
    local offset = enemyGenerator.offset;
    local xPart = math.random(0,1);
    local yPart = math.random(0,1);
  
    if (xPart == 1) then
      if (yPart == 1) then
        point.x = math.random(0, display.contentWidth);
        point.y = math.random(-offset, 0);
      else
      point.x = math.random(-offset, 0);
      point.y = math.random(0, display.contentHeight);
      end
    else
      if (yPart == 1) then
        point.x = math.random(0, display.contentWidth);
        point.y = math.random(display.contentHeight, display.contentHeight + offset);
      else
        point.x = math.random(display.contentWidth, display.contentWidth + offset)
        point.y = math.random(0, display.contentHeight);
      end
    end
    return point;
end
return enemyGenerator